import java.util.Scanner;

import interfaces.Phone;

public class Mainapp {
	
	public static void main(String[] args) {
//	membuat  object phone menggunakan instansi dari class Xiaomi
	Phone redmiNote10 = new Xiaomi();
	
//	membuat object PhoneUser
	PhoneUser andi = new PhoneUser(redmiNote10);
	
//	Nyalakan dulu HP
	andi.turnOnThePhone();
	
//	MEMBUAT TAMPILAN
	Scanner input = new Scanner(System.in);
	String aksi;
	boolean isLooping = true;
	
	do {
		System.out.println("===Aplikasi Interface");
		System.out.println("[1] Nyalakan HP");
		System.out.println("[2] Matikan HP");
		System.out.println("[3] Perbesar volume");
		System.out.println("[4] Perkecil volume");
		System.out.println("[0] Keluar");
		System.out.println("=====================");
		
		System.out.println("Pilih Aksi : ");
		aksi = input.next();
		
		if(aksi.equalsIgnoreCase("1")) {
			andi.turnOnThePhone();
		}else if(aksi.equalsIgnoreCase("2")) {
		    andi.turnOffThePhone();
		}else if(aksi.equalsIgnoreCase("3")) {
			andi.makePhoneLouder();
		}else if(aksi.equalsIgnoreCase("4")) {
			andi.makePhoneSilent();
		}else if(aksi.equalsIgnoreCase("0")) {
			isLooping = false;
			
		}else {
			System.out.println("Aksi yang anda pilih tidak tersedia.");
			System.out.println("Silahkan pilih Aksi kembali");
		}
			
			
	}while(isLooping);
	
	}

}
